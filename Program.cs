﻿/**
*
* Nom programme : AppConsoleBanque.cs
* Version : 1.0
* Date de création : JJ.MM.AAAA
* Date de modification : JJ.MM.AAAA
* Auteur : KORUTOS Noah
* Commentaires : 
*
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using libBanque;

namespace AppConsoleBanque
{
    class Program
    {
        static void Main(string[] args)
        {
            // Création d'un objet CompteBancaire
            CompteBancaire cbPatrick = new CompteBancaire("0211651079B", "M. JANE Patrick");
            CompteBancaire cbTeresa = new CompteBancaire("123121231D", "M. LISBON Teresa", 1500.00m);

            // Affichage du contenu des propriétés
            Console.WriteLine("Numéro du compte : " + cbPatrick.NumCompte + " de " + cbPatrick.NomTitulaire + " :");
            Console.WriteLine(" --> Solde :" + cbPatrick.SoldeCompte);
            Console.WriteLine("Consultation du compte : " + cbTeresa.NumCompte + " de " + cbTeresa.NomTitulaire + " :");
            Console.WriteLine(" --> Solde : " + cbTeresa.SoldeCompte);

            Console.WriteLine("\nDébiter le compte de Mle LISBON de 500 Euros :");
            if (cbTeresa.DebiterCompte(500.00m) == true)
            {
                Console.WriteLine("Ok pour créditer le compte de M.JANE Patrick de 400 Euros :");
                cbPatrick.CrediterCompte(400.00m);
            }
            else
            {
                Console.WriteLine("Opération impossible car solde insuffisant !!!");
            }

            // Consulation des comptes après opérations
            Console.WriteLine();
            Console.WriteLine("Consultation du compte " + cbTeresa.NumCompte + " de " + cbTeresa.NomTitulaire + " :");
            Console.WriteLine(" --> Solde : " + cbTeresa.SoldeCompte);
            Console.WriteLine("Numéro du compte : " + cbPatrick.NumCompte + " de " + cbPatrick.NomTitulaire + " :");
            Console.WriteLine(" --> Solde : " + cbPatrick.SoldeCompte);
        }
    }
}
