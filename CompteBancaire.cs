﻿/**
*
* Nom programme : CompteBancaire.cs
* Version : 1.0
* Date de création : JJ.MM.AAAA
* Date de modification : JJ.MM.AAAA
* Auteur : KORUTOS Noah
* Commentaires : à référencer avec AppConsoleBanque.cs
*
*/
namespace libBanque
{
    public class CompteBancaire
    {
        #region Propriétés privées de la classe

        // Propriétés privées
        private string numCompte;
        private string nomTitulaire;
        private Decimal soldeCompte;

        #endregion

        #region Property = Accesseurs (publiques) aux propriétés

        // Property = Accesseur (publiques) aux propriétés
        /// <summary>
        /// Obtient ou définit le numéro de compte
        /// </summary>
        public string NumCompte { get => numCompte; set => numCompte = value; }

        /// <summary>
        /// Obtient ou définit le nom du titulaire du compte
        /// </summary>  
        public string NomTitulaire { get => nomTitulaire; set => nomTitulaire = value; }

        /// <summary>
        /// Obtient le solde du compte bancaire
        /// </summary>
        public Decimal SoldeCompte { get => soldeCompte; }

        #endregion
        #region Constructeurs de la classe
        /// <summary>
        /// Initialise une nouvelle Instance de la classe libBanque.CompteBancaire
        /// </summary>
        /// <param name="sonNumero">Le numéro du compte bancaire</param>
        /// <param name="sonTitulaire">Le nom du titulaire du compte : nom + prénom</param>
        /// <remarks>Le solde du compte sera initialisé à 0</remarks>
        public CompteBancaire(string sonNumero, string sonTitulaire)
        {
            numCompte = sonNumero;
            nomTitulaire = sonTitulaire;
            soldeCompte = 0;
        }

        public CompteBancaire(string sonNumero, string sonTitulaire, Decimal sonSoldeInitial)
        {
            numCompte = sonNumero;
            nomTitulaire = sonTitulaire;
            soldeCompte = sonSoldeInitial;
        }

        #endregion

        public bool DebiterCompte(decimal a)
        {
            if (soldeCompte > a )
            {
                soldeCompte = soldeCompte - a;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void CrediterCompte(decimal b)
        {
            if ( b > 0)
            {
                soldeCompte = soldeCompte + b;
            }
            else
            {
                Console.WriteLine("Opération impossible car on ne peut pas créditer un nombre negatif !");
            }
        }
        

    }
}